import dao.impl.CustomerDao;
import dao.impl.OrderDao;
import dao.impl.UserDao;


public class Main {
    public static void main(String[] args) {
        OrderDao orderDao = new OrderDao();
        UserDao userDao = new UserDao();
        CustomerDao customerDao = new CustomerDao();

        // FIND ALL

        // userDao.findAll().forEach(user -> System.out.println(user.getUsername()));
        // orderDao.findAll().forEach(order -> System.out.println(order.getTva()));
        // customerDao.findAll().forEach(customer -> System.out.println(customer.getMail()));

        // FIND BY ID

        userDao.findById(1).ifPresent(user -> System.out.println(user.getUsername()));
        orderDao.findById(1).ifPresent(order -> System.out.println(order.getTva()));
        customerDao.findById(1).ifPresent(customer -> System.out.println(customer.getFirstname()));

        // DELETE BY ID

        // userDao.deleteById(1);
        // orderDao.deleteById(1);
        // customerDao.deleteById(1);
    }
}