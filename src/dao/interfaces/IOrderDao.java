package dao.interfaces;

import models.Order;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Optional;

public interface IOrderDao {
    ArrayList<Order> findAll();
    Optional<Order> findById(int id);
    void save(Order order);
    void deleteById(int id);
    Order dataToObject(ResultSet resultSet) throws SQLException;
}
