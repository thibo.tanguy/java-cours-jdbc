package dao.interfaces;

import models.Customer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Optional;

public interface ICustomerDao {
    ArrayList<Customer> findAll();
    Optional<Customer> findById(int id);
    void save(Customer customer);
    void deleteById(int id);
    Customer dataToObject(ResultSet resultSet) throws SQLException;
}
