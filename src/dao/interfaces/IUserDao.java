package dao.interfaces;

import models.User;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Optional;

public interface IUserDao {
    ArrayList<User> findAll();
    Optional<User> findById(int id);
    void save(User user);
    void deleteById(int id);
    User dataToObject(ResultSet resultSet) throws SQLException;
}
