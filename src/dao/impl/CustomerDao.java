package dao.impl;

import dao.interfaces.ICustomerDao;
import models.Customer;
import utils.DbConnexion;

import java.sql.*;
import java.util.ArrayList;
import java.util.Optional;

public class CustomerDao implements ICustomerDao {
    private static final Connection connection;

    static {
        try {
            connection = DbConnexion.getConnection();
        }

        catch (SQLException e) {
            throw new RuntimeException("Failed to establish database connection.", e);
        }
    }

    public ArrayList<Customer> findAll() {
        ArrayList<Customer> customers = new ArrayList<>();
        String request = "SELECT * FROM customers;";

        try (Statement statement = connection.createStatement(); ResultSet resultSet = statement.executeQuery(request)) {
            while (resultSet.next()) {
                customers.add(dataToObject(resultSet));
            }
        }

        catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return customers;
    }

    public Optional<Customer> findById(int id) {
        String request = "SELECT * FROM customers WHERE id = ?;";

        try (PreparedStatement statement = connection.prepareStatement(request)) {
            statement.setInt(1, id);

            try (ResultSet resultSet = statement.executeQuery()) {
                return resultSet.next() ? Optional.of(dataToObject(resultSet)) : Optional.empty();
            }
        }

        catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return Optional.empty();
    }

    public void save(Customer customer) {
        String request = "INSERT INTO customers (lastname, firstname, company, mail, phone, mobile, notes, active) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

        try (PreparedStatement statement = connection.prepareStatement(request)) {
            statement.setString(1, customer.getLastname());
            statement.setString(2, customer.getFirstname());
            statement.setString(3, customer.getCompany());
            statement.setString(4, customer.getMail());
            statement.setString(5, customer.getPhone());
            statement.setString(6, customer.getMobile());
            statement.setString(7, customer.getNotes());
            statement.setBoolean(8, customer.isActive());

            statement.executeUpdate();
        }

        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void deleteById(int id) {
        String request = "DELETE FROM customers WHERE id = ?;";

        try (PreparedStatement statement = connection.prepareStatement(request)) {
            statement.setInt(1, id);
            statement.executeUpdate();
        }

        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public Customer dataToObject(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt("id");
        String lastname = resultSet.getString("lastname");
        String firstname = resultSet.getString("firstname");
        String company = resultSet.getString("company");
        String mail = resultSet.getString("mail");
        String phone = resultSet.getString("phone");
        String mobile = resultSet.getString("mobile");
        String notes = resultSet.getString("notes");
        boolean active = resultSet.getBoolean("active");

        return new Customer(id, lastname, firstname, company, mail, phone, mobile, notes, active);
    }
}
