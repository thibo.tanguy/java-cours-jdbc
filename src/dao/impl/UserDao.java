package dao.impl;

import dao.interfaces.IUserDao;
import models.User;
import utils.DbConnexion;

import java.sql.*;
import java.util.ArrayList;
import java.util.Optional;

public class UserDao implements IUserDao {
    private static final Connection connection;

    static {
        try {
            connection = DbConnexion.getConnection();
        } catch (SQLException e) {
            throw new RuntimeException("Failed to establish database connection.", e);
        }
    }

    public ArrayList<User> findAll() {
        ArrayList<User> users = new ArrayList<>();
        String request = "SELECT * FROM users;";

        try (Statement statement = connection.createStatement(); ResultSet resultSet = statement.executeQuery(request)) {
            while (resultSet.next()) {
                users.add(dataToObject(resultSet));
            }
        }

        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return users;
    }

    public Optional<User> findById(int id) {
        String request = "SELECT * FROM users WHERE id = ?;";

        try (PreparedStatement statement = connection.prepareStatement(request)) {
            statement.setInt(1, id);

            try (ResultSet resultSet = statement.executeQuery()) {
                return resultSet.next() ? Optional.of(dataToObject(resultSet)) : Optional.empty();
            }
        }

        catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return Optional.empty();
    }

    public void save(User user) {
        String request = "INSERT INTO users (username, password, mail, grants) VALUES (?, ?, ?, ?)";

        try (PreparedStatement statement = connection.prepareStatement(request)) {
            statement.setString(1, user.getUsername());
            statement.setString(2, user.getPassword());
            statement.setString(3, user.getMail());
            statement.setString(4, user.getGrants());

            statement.executeUpdate();
        }

        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void deleteById(int id) {
        String request = "DELETE FROM users WHERE id = ?;";

        try (PreparedStatement statement = connection.prepareStatement(request)) {
            statement.setInt(1, id);
            statement.executeUpdate();
        }

        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public User dataToObject(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt("id");
        String username = resultSet.getString("username");
        String password = resultSet.getString("password");
        String mail = resultSet.getString("mail");
        String grants = resultSet.getString("grants");

        return new User(id, username, password, mail, grants);
    }
}
