package dao.impl;

import dao.interfaces.IOrderDao;
import models.Order;
import utils.DbConnexion;

import java.sql.*;
import java.util.ArrayList;
import java.util.Optional;

public class OrderDao implements IOrderDao {
    private static final Connection connection;

    static {
        try {
            connection = DbConnexion.getConnection();
        }

        catch (SQLException e) {
            throw new RuntimeException("Failed to establish database connection.", e);
        }
    }

    public ArrayList<Order> findAll() {
        ArrayList<Order> orders = new ArrayList<>();
        String request = "SELECT * FROM orders;";

        try (Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(request)) {
            while (resultSet.next()) {
                orders.add(dataToObject(resultSet));
            }
        }

        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return orders;
    }

    public Optional<Order> findById(int id) {
        String request = "SELECT * FROM orders WHERE id = ?;";

        try (PreparedStatement statement = connection.prepareStatement(request)) {
            statement.setInt(1, id);

            try (ResultSet resultSet = statement.executeQuery()) {
                return resultSet.next() ? Optional.of(dataToObject(resultSet)) : Optional.empty();
            }
        }

        catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        return Optional.empty();
    }

    public void save(Order order) {
        String request = "INSERT INTO orders (customer_id, label, adr_et, number_of_days, tva, status, type, notes) VALUES (?, ?, ?, ?, ?, ?, ?, ?);";

        try (PreparedStatement statement = connection.prepareStatement(request)) {
            statement.setInt(1, order.getCustomerId());
            statement.setString(2, order.getLabel());
            statement.setInt(3, order.getAdrEt());
            statement.setInt(4, order.getNumberOfDays());
            statement.setInt(5, order.getTva());
            statement.setString(6, order.getStatus());
            statement.setString(7, order.getType());
            statement.setString(8, order.getNotes());

            statement.executeUpdate();
        }

        catch (SQLException e) {
            System.out.println(e.getMessage());

        }
    }

    public void deleteById(int id) {
        String request = "DELETE FROM orders WHERE id = ?;";

        try (PreparedStatement statement = connection.prepareStatement(request)) {
            statement.setInt(1, id);
            statement.executeUpdate();
        }

        catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public Order dataToObject(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt("id");
        int customerId = resultSet.getInt("customer_id");
        String label = resultSet.getString("label");
        int adr_et = resultSet.getInt("adr_et");
        int number_of_days = resultSet.getInt("number_of_days");
        int tva = resultSet.getInt("tva");
        String status = resultSet.getString("status");
        String type = resultSet.getString("type");
        String notes = resultSet.getString("notes");

        return new Order(id, customerId, label, adr_et, number_of_days, tva, status, type, notes);
    }
}
