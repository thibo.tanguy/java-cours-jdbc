package models;

public class Customer {
    private int id;
    private String lastname;
    private String firstname;
    private String company;
    private String mail;
    private String phone;
    private String mobile;
    private String notes;
    private boolean active;

    public Customer(int id, String lastname, String firstname, String company, String mail, String phone, String mobile, String notes, boolean active) {
       this.id = id;
       this.lastname = lastname;
       this.firstname = firstname;
       this.company = company;
       this.mail = mail;
       this.phone = phone;
       this.mobile = mobile;
       this.notes = notes;
       this.active = active;
    }

   public int getId() {
      return id;
   }

   public String getFirstname() {
      return firstname;
   }

   public String getLastname() {
      return lastname;
   }

   public String getCompany() {
      return company;
   }

   public String getMail() {
      return mail;
   }

   public String getMobile() {
      return mobile;
   }

   public String getPhone() {
      return phone;
   }

   public String getNotes() {
      return notes;
   }

   public boolean isActive() {
      return active;
   }

   public void setId(int id) {
      this.id = id;
   }

   public void setFirstname(String firstname) {
      this.firstname = firstname;
   }

   public void setLastname(String lastname) {
      this.lastname = lastname;
   }

   public void setCompany(String company) {
      this.company = company;
   }

   public void setMail(String mail) {
      this.mail = mail;
   }

   public void setPhone(String phone) {
      this.phone = phone;
   }

   public void setMobile(String mobile) {
      this.mobile = mobile;
   }

   public void setNotes(String notes) {
      this.notes = notes;
   }

   public void setActive(boolean active) {
      this.active = active;
   }
}
