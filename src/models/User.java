package models;

public class User {
    private int id;
    private String username;
    private String password;
    private String mail;
    private String grants;

    public User(int id, String username, String password, String mail, String grants) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.mail = mail;
        this.grants = grants;
    }

    public int getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getMail() {
        return mail;
    }

    public String getGrants() {
        return grants;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public void setGrants(String grants) {
        this.grants = grants;
    }
}