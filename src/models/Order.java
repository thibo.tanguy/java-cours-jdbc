package models;

public class Order  {
    private int id;
    private int customerId;
    private String label;
    private int adrEt;
    private int numberOfDays;
    private int tva;
    private String status;
    private String type;
    private String notes;

    public Order(int id, int customerId, String label, int adrEt, int numberOfDays, int tva, String status, String type, String notes) {
        this.id = id;
        this.customerId = customerId;
        this.label = label;
        this.adrEt = adrEt;
        this.numberOfDays = numberOfDays;
        this.tva = tva;
        this.status = status;
        this.type = type;
        this.notes = notes;
    }

    public int getId() {
        return id;
    }

    public int getCustomerId() {
        return customerId;
    }

    public String getLabel() {
        return label;
    }

    public int getAdrEt() {
        return adrEt;
    }

    public int getNumberOfDays() {
        return numberOfDays;
    }

    public int getTva() {
        return tva;
    }

    public String getStatus() {
        return status;
    }

    public String getType() {
        return type;
    }

    public String getNotes() {
        return notes;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public void setAdrEt(int adrEt) {
        this.adrEt = adrEt;
    }

    public void setNumberOfDays(int numberOfDays) {
        this.numberOfDays = numberOfDays;
    }

    public void setTva(int tva) {
        this.tva = tva;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}