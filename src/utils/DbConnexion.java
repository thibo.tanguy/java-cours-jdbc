package utils;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbConnexion {
    private static Connection connection;
    private static final String DB_HOST = "localhost";
    private static final int PORT = 5432;
    private static final String DB_NAME = "crm";
    private static final String DB_USER = "crm";
    private static final String DB_PASSWORD = "crm";
    private static final String CONNECTION_URL = "jdbc:postgresql://" + DB_HOST + ":" + PORT + "/" + DB_NAME;

    public static Connection getConnection() throws SQLException {
        if (DbConnexion.connection == null) DbConnexion.connection = DriverManager.getConnection(CONNECTION_URL, DB_USER, DB_PASSWORD);

        return DbConnexion.connection;
    }
}
